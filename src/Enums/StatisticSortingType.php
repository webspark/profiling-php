<?php

declare(strict_types=1);

namespace Webspark\Profiling\Enums;

use InvalidArgumentException;

class StatisticSortingType
{
    public const MAX = 'max';
    public const MIN = 'min';
    public const AVG = 'avg';
    public const MED = 'med';
    public const TOTAL = 'total';
    public const CALLS = 'calls';

    private string $type;

    public function __construct(string $type)
    {
        if (in_array($type, [self::MAX, self::MIN, self::AVG, self::MED, self::TOTAL, self::CALLS], true) === false) {
            throw new InvalidArgumentException('Invalid sorting type');
        }

        $this->type = $type;
    }

    public function getType(): string
    {
        return $this->type;
    }
}
