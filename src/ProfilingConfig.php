<?php

declare(strict_types=1);

namespace Webspark\Profiling;

class ProfilingConfig
{
    protected float $latency = 0;

    public function __construct(array $config = [])
    {
        $this->latency = isset($config['latency']) ? (float) $config['latency'] : $this->latency;
    }

    public function getLatency(): float
    {
        return $this->latency;
    }
}