<?php

declare(strict_types=1);

namespace Webspark\Profiling\Processors;

use Webspark\Profiling\Dto\ProfilingRow;

class InMemoryProfilingProcessor implements ProfilingProcessorInterface
{
    /** @var ProfilingRow[] */
    protected array $rows = [];

    public function write(ProfilingRow $profilingRow): void
    {
        $this->rows[] = $profilingRow;
    }

    public function rows(): array
    {
        return $this->rows;
    }
}