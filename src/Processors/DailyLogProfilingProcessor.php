<?php

declare(strict_types=1);

namespace Webspark\Profiling\Processors;

use Webspark\Profiling\Dto\ProfilingRow;
use UnexpectedValueException;
use SplFileObject;

class DailyLogProfilingProcessor implements ProfilingProcessorInterface
{
    protected string $name;
    protected string $folder;
    protected int $storingDays;

    private ?bool $mustRotate = null;
    private ?bool $dirCreated = null;
    private ?string $errorMessage = null;

    public function __construct(string $name, string $folder, int $storingDays = 7)
    {
        $this->name = $name;
        $this->folder = rtrim($folder, '/') . '/';
        $this->storingDays = $storingDays;
    }

    protected function fileName(int $timestamp): string
    {
        return $this->name . '-' . date('Y-m-d', $timestamp) . '.log';
    }

    protected function globPattern(): string
    {
        return $this->folder . $this->name . '-[0-9][0-9][0-9][0-9]-[0-9][0-9]-[0-9][0-9].log';
    }

    public function rotate(): void
    {
        if ($this->storingDays === 0) {
            return;
        }

        $logFiles = glob($this->globPattern()) ?: [];

        if ($this->storingDays >= count($logFiles)) {
            // no files to remove
            return;
        }

        // Sorting the files by name to remove the older ones
        usort($logFiles, function (string $first, string $second) {
            return strcmp($second, $first);
        });

        foreach (array_slice($logFiles, $this->storingDays) as $file) {
            if (is_writable($file)) {
                // @codeCoverageIgnoreStart
                // suppress errors here as unlink() might fail if two processes
                // are cleaning up/rotating at the same time
                set_error_handler(function (): bool {
                    return false;
                });

                unlink($file);
                restore_error_handler();
                // @codeCoverageIgnoreEnd
            }
        }

        $this->mustRotate = false;
    }

    private function customErrorHandler(int $code, string $msg): bool
    {
        // @codeCoverageIgnoreStart
        $this->errorMessage = preg_replace('{^(fopen|mkdir)\(.*?\): }', '', $msg);

        return true;
        // @codeCoverageIgnoreEnd
    }

    private function createDir(string $dirPath): void
    {
        // Do not try to create dir if it has already been tried.
        if ($this->dirCreated === true) {
            return;
        }

        if (is_dir($dirPath) === false) {
            $this->errorMessage = null;
            set_error_handler([$this, 'customErrorHandler']);

            $oldUmask = umask(0);
            $status = mkdir($dirPath, 0777, true);
            umask($oldUmask);

            restore_error_handler();

            // @codeCoverageIgnoreStart

            if (false === $status && is_dir($dirPath) === false && strpos((string) $this->errorMessage, 'File exists') === false) {
                throw new UnexpectedValueException(sprintf('There is no existing directory at "%s" and it could not be created: ' . $this->errorMessage, $dirPath));
            }

            // @codeCoverageIgnoreEnd
        }

        $this->dirCreated = true;
    }

    private function appendContents(string $filePath, string $content): void
    {
        $this->createDir(dirname($filePath));

        $this->errorMessage = null;
        set_error_handler([$this, 'customErrorHandler']);

        try {
            file_put_contents($filePath, $content, FILE_APPEND | LOCK_EX) !== false;
        } finally {
            restore_error_handler();
        }
    }

    public function write(ProfilingRow $profilingRow): void
    {
        $filePath = $this->folder . $this->fileName($profilingRow->timestamp);

        // on the first record written, if the log is new, we should rotate (once per day)
        if ($this->mustRotate === null) {
            $this->mustRotate = file_exists($filePath) === false;
        }

        $message = '[' . date('Y-m-d H:i:s', $profilingRow->timestamp) . '] ' . json_encode([
            'action' => $profilingRow->action,
            'timestamp' => $profilingRow->timestamp,
            'context' => $profilingRow->context,
        ]) . PHP_EOL;

        $this->appendContents($filePath, $message);
    }

    public function rows(): array
    {
        $logFiles = glob($this->globPattern());
        $rows = [];

        foreach ($logFiles as $logFile) {
            if (is_file($logFile) === false) {
                continue;
            }

            $file = new SplFileObject($logFile);

            while (!$file->eof()) {
                $line = trim($file->getCurrentLine());

                if (empty($line)) {
                    continue;
                }

                $content = json_decode(preg_replace('/\[[^]]+] /', '', $line), true);
                $rows[] = new ProfilingRow(
                    (string) $content['action'],
                    (int) $content['timestamp'],
                    $content['context'],
                );
            }
        }

        return $rows;
    }

    public function __destruct()
    {
        if ($this->mustRotate) {
            $this->rotate();
        }
    }
}