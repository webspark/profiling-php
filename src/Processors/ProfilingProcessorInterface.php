<?php

declare(strict_types=1);

namespace Webspark\Profiling\Processors;

use Webspark\Profiling\Dto\ProfilingRow;

interface ProfilingProcessorInterface
{
    public function write(ProfilingRow $profilingRow): void;

    /**
     * @return ProfilingRow[]
     */
    public function rows(): array;
}
