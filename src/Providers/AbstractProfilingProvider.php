<?php

declare(strict_types=1);

namespace Webspark\Profiling\Providers;

use Webspark\Profiling\ProfilingConfig;
use Exception;

abstract class AbstractProfilingProvider
{
    private static array $instances = [];
    protected ProfilingConfig $config;

    protected function __construct()
    {
        $this->config = new ProfilingConfig();
    }

    /**
     * @throws Exception
     */
    public function __clone()
    {
        throw new Exception('Cannot clone a provider.');
    }

    /**
     * @throws Exception
     */
    public function __wakeup()
    {
        throw new Exception('Cannot unserialize a provider.');
    }

    /**
     * @return static
     */
    public static function getInstance(): self
    {
        $class = static::class;

        if (!isset(self::$instances[$class])) {
            self::$instances[$class] = new static();
        }

        return self::$instances[$class];
    }

    public function getConfig(): ProfilingConfig
    {
        return $this->config;
    }

    public function setConfig(ProfilingConfig $config): self
    {
        $this->config = $config;
        return $this;
    }
}