<?php

declare(strict_types=1);

namespace Webspark\Profiling\Providers;

use Webspark\Profiling\Processors\ProfilingProcessorInterface;
use Webspark\Profiling\Processors\InMemoryProfilingProcessor;
use Webspark\Profiling\ProfilingConfig;

/**
 * @method static static getInstance()
 */
class SpeedProfilingProvider extends AbstractProfilingProvider
{
    protected ProfilingProcessorInterface $processor;

    protected function __construct()
    {
        parent::__construct();

        $this->config = new ProfilingConfig(['latency' => 1000]);
        $this->processor = new InMemoryProfilingProcessor();
    }

    public function getProcessor(): ProfilingProcessorInterface
    {
        return $this->processor;
    }

    public function setProcessor(ProfilingProcessorInterface $processor): self
    {
        $this->processor = $processor;
        return $this;
    }
}