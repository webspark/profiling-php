<?php

declare(strict_types=1);

namespace Webspark\Profiling\Providers;

use Webspark\Profiling\Helpers;

/**
 * @method static static getInstance()
 */
class TimingsProfilingProvider extends AbstractProfilingProvider
{
    private string $eventNameBootstrap = 'Bootstrap';
    private string $eventNameTotal = 'Total';
    private string $eventNameApp = 'App';

    protected array $finishedEvents = [];
    protected array $startedEvents = [];
    protected ?float $startTime = null;

    protected function getStartTime(): float
    {
        return $_SERVER['REQUEST_TIME_FLOAT'] ?? $this->now();
    }

    protected function now(): float
    {
        return microtime(true);
    }

    protected function timeFromStart(): float
    {
        if ($this->startTime === null) {
            $this->startTime = $this->getStartTime();
        }

        return ($this->now() - $this->startTime) * 1000;
    }

    protected function generateHeaders(): string
    {
        $systemEvents = [$this->eventNameTotal, $this->eventNameBootstrap, $this->eventNameApp];
        $latency = $this->config->getLatency();
        $headerParts = [];

        foreach ($this->finishedEvents as $eventName => $duration) {
            if (in_array($eventName, $systemEvents, true) === false && $duration < $latency) {
                continue;
            }

            $eventNameSlug = Helpers::slug($eventName);

            $header = $eventNameSlug . ';desc="' . $eventName . '";';

            if (is_null($duration) === false) {
                $header .= 'dur=' . $duration;
            }

            $headerParts[] = $header;
        }

        return implode(', ', $headerParts);
    }

    public function header(): string
    {
        $this->stopAllUnfinishedEvents();

        $this->setDuration($this->eventNameTotal, $this->timeFromStart());

        return 'Server-Timing: ' . $this->generateHeaders();
    }

    public function init(?float $startTime = null): void
    {
        if ($startTime) {
            $this->startTime = $startTime;
        }

        $this->setDuration($this->eventNameBootstrap, $this->timeFromStart());
        $this->start($this->eventNameApp);
    }

    public function start(string $timingName): void
    {
        $this->startedEvents[$timingName] = $this->now();
    }

    public function stop(string $timingName): void
    {
        if (isset($this->startedEvents[$timingName])) {
            $timeDifference = ($this->now() - $this->startedEvents[$timingName]) * 1000;

            $this->setDuration($timingName, $timeDifference);

            unset($this->startedEvents[$timingName]);
        }
    }

    public function stopAllUnfinishedEvents(): void
    {
        foreach (array_keys($this->startedEvents) as $timingName) {
            $this->stop($timingName);
        }
    }

    /**
     * @param string $timingName
     * @param callable|float $duration
     * @return $this
     */
    public function setDuration(string $timingName, $duration): self
    {
        if (is_callable($duration)) {
            $this->start($timingName);

            call_user_func($duration);

            $this->stop($timingName);
        } elseif (isset($this->finishedEvents[$timingName])) {
            $this->finishedEvents[$timingName] += $duration;
        } else {
            $this->finishedEvents[$timingName] = $duration;
        }

        return $this;
    }

    public function getDuration(string $timingName): ?float
    {
        return $this->finishedEvents[$timingName] ?? null;
    }

    /**
     * @deprecated Use setDuration instead
     */
    public function increment(string $timingName, float $microTime): void
    {
        $this->setDuration($timingName, $microTime);
    }

    public function timings(): array
    {
        return $this->finishedEvents;
    }
}