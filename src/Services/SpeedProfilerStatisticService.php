<?php

declare(strict_types=1);

namespace Webspark\Profiling\Services;

use Webspark\Profiling\Providers\SpeedProfilingProvider;
use Webspark\Profiling\Dto\ProfilerStatisticFilters;
use Webspark\Profiling\Enums\StatisticSortingType;
use Webspark\Profiling\Dto\ProfilingRow;

class SpeedProfilerStatisticService
{
    public function generate(ProfilerStatisticFilters $filters): array
    {
        $latencyData = $this->parse($filters->date);

        # Get average exec-time by method, min, max, total execution.
        foreach ($latencyData as $action => $statistic) {
            $executions = array_column($statistic['source'], 'exec');

            $latencyData[$action]['action'] = $action;
            $latencyData[$action]['calls'] = count($executions);
            $latencyData[$action]['total'] = round(array_sum($executions), 2);
            $latencyData[$action]['avg'] = round(array_sum($executions) / count($executions), 2);
            $latencyData[$action]['med'] = $this->calculateMedian($executions);

            $latencyData[$action]['min'] = min($executions);
            $latencyData[$action]['max'] = max($executions);

            foreach ($statistic['source'] as $source) {
                foreach ($source['timings'] as $timingType => $timeInMs) {
                    if (!array_key_exists($timingType, $latencyData[$action]['timings_temp'])) {
                        $latencyData[$action]['timings_temp'][$timingType]['time'] = $timeInMs;
                        $latencyData[$action]['timings_temp'][$timingType]['count'] = 1;
                    } else {
                        $latencyData[$action]['timings_temp'][$timingType]['time'] += $timeInMs;
                        $latencyData[$action]['timings_temp'][$timingType]['count']++;
                    }
                }
            }

            foreach ($latencyData[$action]['timings_temp'] as $timingType => $data) {
                $latencyData[$action]['timings'][$timingType] = round($data['time'] / $data['count'], 2);
            }

            unset($latencyData[$action]['source'], $latencyData[$action]['timings_temp']);
        }

        $sortBy = $filters->sort ? $filters->sort->getType() : StatisticSortingType::MAX;

        uasort($latencyData, static function(array $first, array $second) use ($sortBy): int {
            if ($first[$sortBy] === $second[$sortBy]) {
                return 0;
            }

            return ($first[$sortBy] > $second[$sortBy]) ? -1 : 1;
        });

        return array_slice($latencyData, 0, $filters->limit);
    }

    protected function calculateMedian(array $executions): float
    {
        $count = count($executions);
        $percentileCount = (int) floor($count / 100 * 10);

        if ($percentileCount) {
            $medianExecutions = array_slice($executions, $percentileCount);
            $medianExecutions = array_slice($medianExecutions, 0, -1 * $percentileCount);
        } else {
            $medianExecutions = $executions;
        }

        return round(array_sum($medianExecutions) / count($medianExecutions), 2);
    }

    protected function parse(?string $date): array
    {
        $processor = SpeedProfilingProvider::getInstance()->getProcessor();
        $latencyData = [];

        if ($date) {
            $rows = array_filter(
                $processor->rows(),
                static function (ProfilingRow $row) use ($date): bool {
                    return date('Y-m-d', $row->timestamp) === $date;
                }
            );
        } else {
            $rows = $processor->rows();
        }

        foreach ($rows as $row) {
            $latencyData[$row->action]['source'][] = [
                'timestamp' => $row->timestamp,
                'exec' => $row->context['exec-time'] ?? 0,
                'timings' => $row->context['timings'] ?? [],
            ];

            $latencyData[$row->action]['timings_temp'] = [];
        }

        return $latencyData;
    }
}