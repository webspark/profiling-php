<?php

declare(strict_types=1);

namespace Webspark\Profiling\Profilers;

use Webspark\Profiling\Providers\SpeedProfilingProvider;
use Webspark\Profiling\Dto\ProfilingRow;
use Webspark\Profiling\Providers\TimingsProfilingProvider;

class SpeedProfiler
{
    protected SpeedProfilingProvider $provider;
    protected array $resourceUsage;
    protected float $started;
    protected string $action;

    public function __construct(string $action = '')
    {
        $this->provider = SpeedProfilingProvider::getInstance();
        $this->resourceUsage = getrusage() ?: [];
        $this->started = $this->now();
        $this->action = $action;

        if (empty($action)) {
            $this->action = $_SERVER['REQUEST_URI'] ?? '-';
            $this->action = preg_replace('/\?.*/', '', $this->action);
        }
    }

    public function checkpoint(bool $resetTime = true, string $suffix = ''): void
    {
        $timeDifference = ($this->now() - $this->started) * 1000;
        $resourceUsage = getrusage() ?: [];

        $currentSecDifference = $resourceUsage['ru_utime.tv_sec'] * 1e6 + $resourceUsage['ru_utime.tv_usec'];
        $startedSecDifference = $this->resourceUsage['ru_utime.tv_sec'] * 1e6 + $this->resourceUsage['ru_utime.tv_usec'];
        $userSecDifference = $resourceUsage['ru_stime.tv_usec'] - $this->resourceUsage['ru_stime.tv_usec'];

        $userTime = ($currentSecDifference - $startedSecDifference) / 1000;
        $systemTime = ($userSecDifference) / 1000;

        if ($timeDifference > $this->provider->getConfig()->getLatency()) {
            $this->provider->getProcessor()->write(new ProfilingRow(
                $this->action . ($suffix ? (' ' . $suffix) : ''),
                time(),
                array_merge(
                    [
                        'user-time' => round($userTime, 3),
                        'system-time' => round($systemTime, 3),
                        'exec-time' => round($timeDifference, 3),
                        'oublock' => $resourceUsage['ru_oublock'] - $this->resourceUsage['ru_oublock'],
                        'inblock' => $resourceUsage['ru_inblock'] - $this->resourceUsage['ru_inblock'],
                        'ru_msgsnd' => $resourceUsage['ru_msgsnd'] - $this->resourceUsage['ru_msgsnd'],
                        'ru_msgrcv' => $resourceUsage['ru_msgrcv'] - $this->resourceUsage['ru_msgrcv'],
                        'ru_maxrss' => ($resourceUsage['ru_maxrss'] - $this->resourceUsage['ru_maxrss']) / 1024,
                        'ru_ixrss' => ($resourceUsage['ru_ixrss'] - $this->resourceUsage['ru_ixrss']) / 1024,
                        'ru_idrss' => ($resourceUsage['ru_idrss'] - $this->resourceUsage['ru_idrss']) / 1024,
                        'ru_nsignals' => $resourceUsage['ru_nsignals'] - $this->resourceUsage['ru_nsignals'],
                        'ru_nvcsw' => $resourceUsage['ru_nvcsw'] - $this->resourceUsage['ru_nvcsw'],
                        'ru_nivcsw' => $resourceUsage['ru_nivcsw'] - $this->resourceUsage['ru_nivcsw'],
                    ],
                    $this->additionalContext(),
                ),
            ));
        }

        if ($resetTime) {
            $this->started = $this->now();
        }
    }

    protected function additionalContext(): array
    {
        return [
            'request_uri' => $_SERVER['REQUEST_URI'] ?? '-',
            'timings' => TimingsProfilingProvider::getInstance()->timings(),
            'post_params' => preg_replace('/\n/', '\\\n', json_encode($_POST)),
        ];
    }

    public function __destruct()
    {
        $this->checkpoint();
    }

    protected function now(): float
    {
        return microtime(true);
    }
}
