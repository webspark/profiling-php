<?php

declare(strict_types=1);

namespace Webspark\Profiling\Profilers;

use Webspark\Profiling\Providers\TimingsProfilingProvider;

class TimingsProfiler
{
    protected TimingsProfilingProvider $provider;
    protected string $timingName;
    protected float $started;

    public function __construct(string $timingName)
    {
        $this->provider = TimingsProfilingProvider::getInstance();
        $this->timingName = $timingName;

        $this->provider->start($this->timingName);
    }

    public function __destruct()
    {
        $this->provider->stop($this->timingName);
    }
}
