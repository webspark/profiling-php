<?php

declare(strict_types=1);

namespace Webspark\Profiling\Dto;

class ProfilingRow
{
    public string $action;
    public int $timestamp;
    public array $context;

    public function __construct(string $action, int $timestamp, array $context)
    {
        $this->action = $action;
        $this->timestamp = $timestamp;
        $this->context = $context;
    }
}