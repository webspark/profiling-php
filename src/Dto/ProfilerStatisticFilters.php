<?php

declare(strict_types=1);

namespace Webspark\Profiling\Dto;

use Webspark\Profiling\Enums\StatisticSortingType;

class ProfilerStatisticFilters
{
    public ?StatisticSortingType $sort;
    public int $limit;
    public ?string $date;

    public function __construct(int $limit, ?string $date = null, ?StatisticSortingType $sort = null)
    {
        $this->limit = $limit;
        $this->date = $date;
        $this->sort = $sort;
    }
}
