<?php

/*
|--------------------------------------------------------------------------
| Test Case
|--------------------------------------------------------------------------
|
| The closure you provide to your test functions is always bound to a specific PHPUnit test
| case class. By default, that class is "PHPUnit\Framework\TestCase". Of course, you may
| need to change it using the "uses()" function to bind a different classes or traits.
|
*/

// uses(Tests\TestCase::class)->in('Feature');

/*
|--------------------------------------------------------------------------
| Expectations
|--------------------------------------------------------------------------
|
| When you're writing tests, you often need to check that values meet certain conditions. The
| "expect()" function gives you access to a set of "expectations" methods that you can use
| to assert different things. Of course, you may extend the Expectation API at any time.
|
*/

use Webspark\Profiling\Dto\ProfilingRow;
use Webspark\Profiling\Profilers\SpeedProfiler;
use Webspark\Profiling\Profilers\TimingsProfiler;
use Webspark\Profiling\Providers\SpeedProfilingProvider;

expect()->extend('toBeOne', function () {
    return $this->toBe(1);
});

/*
|--------------------------------------------------------------------------
| Functions
|--------------------------------------------------------------------------
|
| While Pest is very powerful out-of-the-box, you may have some testing code specific to your
| project that you don't want to repeat in every file. Here you can also expose helpers as
| global functions to help you to reduce the number of lines of code in your test files.
|
*/

function timingProfilingCallback(string $action, int $microseconds): bool
{
    $profiling = new TimingsProfiler($action);
    usleep($microseconds);

    return true;
}

function speedProfilingCallback(string $action, int $microseconds): bool
{
    $profiling = new SpeedProfiler($action);
    usleep($microseconds);

    return true;
}

function speedProfilingFindRow(string $action): ?ProfilingRow
{
    $rows = array_filter(
        SpeedProfilingProvider::getInstance()->getProcessor()->rows(),
        static function (ProfilingRow $row) use ($action): bool {
            return $row->action === $action;
        }
    );

    return array_shift($rows);
}
