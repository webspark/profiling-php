<?php

use Webspark\Profiling\Providers\TimingsProfilingProvider;

it('correctly write timings', function () {
    $provider = TimingsProfilingProvider::getInstance();
    $provider->init();

    timingProfilingCallback('Action Profiler Correct', 5000);

    $provider->setDuration('Action Correct', 1000);
    $provider->stopAllUnfinishedEvents();

    expect($provider->getDuration('Bootstrap'))->toBeGreaterThan(0)
        ->and($provider->getDuration('App'))->toBeGreaterThan(0)
        ->and((int) $provider->getDuration('Action Profiler Correct') === 5)->toBeTrue()
        ->and((int) $provider->getDuration('Action Correct') === 1000)->toBeTrue();
});

it('correctly initiated', function () {
    $provider = TimingsProfilingProvider::getInstance();
    $provider->init(microtime(true) - 2000);
    $provider->stopAllUnfinishedEvents();

    expect($provider->getDuration('Bootstrap'))->toBeGreaterThan(2);
});
