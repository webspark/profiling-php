<?php

use Webspark\Profiling\ProfilingConfig;
use Webspark\Profiling\Providers\TimingsProfilingProvider;

it('correctly generate header', function () {
    $provider = TimingsProfilingProvider::getInstance();
    $serverTimingHeader = $provider->header();

    expect(strstr($serverTimingHeader, 'Server-Timing: ') !== false)->toBeTrue()
        ->and(strstr($serverTimingHeader, 'Total') !== false)->toBeTrue()
        ->and(strstr($serverTimingHeader, 'Bootstrap') !== false)->toBeTrue()
        ->and(strstr($serverTimingHeader, 'App') !== false)->toBeTrue();
});

it('can filter events by latency', function () {
    $provider = TimingsProfilingProvider::getInstance();
    $provider->setConfig(new ProfilingConfig(['latency' => 2000]));
    $provider->setDuration('testing-latency-filter', 1000);
    $serverTimingHeader = $provider->header();

    expect(strstr($serverTimingHeader, 'Server-Timing: ') !== false)->toBeTrue()
        ->and(strstr($serverTimingHeader, 'Total') !== false)->toBeTrue()
        ->and(strstr($serverTimingHeader, 'Bootstrap') !== false)->toBeTrue()
        ->and(strstr($serverTimingHeader, 'App') !== false)->toBeTrue()
        ->and(strstr($serverTimingHeader, 'testing-latency-filter') === false)->toBeTrue();
});
