<?php

use Webspark\Profiling\Providers\TimingsProfilingProvider;

it('can start and stop timings', function () {
    $provider = TimingsProfilingProvider::getInstance();

    $provider->start('start-stop');
    usleep(5000);
    $provider->stop('start-stop');

    expect((int) $provider->getDuration('start-stop') === 5)->toBeTrue();
});

it('can stop started timings', function () {
    $provider = TimingsProfilingProvider::getInstance();
    $provider->start('started');
    usleep(1000);

    $provider->stopAllUnfinishedEvents();

    expect((int) $provider->getDuration('started') === 1)->toBeTrue();
});
