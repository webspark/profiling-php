<?php

use Webspark\Profiling\Profilers\TimingsProfiler;
use Webspark\Profiling\Providers\TimingsProfilingProvider;

it('can write timings', function () {
    timingProfilingCallback('callable-profiler', 5000);

    $provider = TimingsProfilingProvider::getInstance();

    expect((int) $provider->getDuration('callable-profiler') === 5)->toBeTrue();
});

it('will not write started timing', function () {
    $profiler = new TimingsProfiler('profiler-started');
    $provider = TimingsProfilingProvider::getInstance();

    expect($provider->getDuration('profiler-started'))->toBeNull();
});
