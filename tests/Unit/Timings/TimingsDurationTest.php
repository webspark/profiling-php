<?php

use Webspark\Profiling\Providers\TimingsProfilingProvider;

it('can set duration', function () {
    $provider = TimingsProfilingProvider::getInstance();
    $provider->setDuration('testing', 1000);

    expect((int) $provider->getDuration('testing') === 1000)->toBeTrue();
});

it('can set multiple durations', function () {
    $provider = TimingsProfilingProvider::getInstance();
    $provider->setDuration('testing_1', 1000);
    $provider->setDuration('testing_2', 2000);

    expect((int) $provider->getDuration('testing_1') === 1000)->toBeTrue()
        ->and((int) $provider->getDuration('testing_2') === 2000)->toBeTrue();
});

it('can set duration with callables', function () {
    $provider = TimingsProfilingProvider::getInstance();
    $provider->setDuration('callable', static function () {
        usleep(1000);
    });

    expect((int) $provider->getDuration('callable') === 1)->toBeTrue();
});

it('can increment durations', function () {
    $provider = TimingsProfilingProvider::getInstance();
    $provider->setDuration('incremental', 1000);
    $provider->setDuration('incremental', 2000);

    expect((int) $provider->getDuration('incremental') === 3000)->toBeTrue();
});

it('can increment durations with increment method', function () {
    $provider = TimingsProfilingProvider::getInstance();
    $provider->increment('incremental-2', 1000);
    $provider->increment('incremental-2', 2000);

    expect((int) $provider->getDuration('incremental-2') === 3000)->toBeTrue();
});
