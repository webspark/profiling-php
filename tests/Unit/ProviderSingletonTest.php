<?php

use Webspark\Profiling\Providers\TimingsProfilingProvider;
use Webspark\Profiling\Providers\SpeedProfilingProvider;

it('can create speed instance and timings instance in one time', function () {
    $timingsProvider = TimingsProfilingProvider::getInstance();
    $speedProvider = SpeedProfilingProvider::getInstance();

    expect($timingsProvider !== $speedProvider)->toBeTrue();
});
