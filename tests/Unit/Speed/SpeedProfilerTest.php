<?php

use Webspark\Profiling\Processors\InMemoryProfilingProcessor;
use Webspark\Profiling\Profilers\SpeedProfiler;
use Webspark\Profiling\ProfilingConfig;
use Webspark\Profiling\Providers\SpeedProfilingProvider;

it('set default action', function () {
    SpeedProfilingProvider::getInstance()
        ->setProcessor(new InMemoryProfilingProcessor())
        ->setConfig(new ProfilingConfig(['latency' => 1]));

    $profiler = new SpeedProfiler();
    usleep(1000);

    unset($profiler);

    $actionRow = speedProfilingFindRow('-');

    expect(empty($actionRow))->toBeFalse()
        ->and((int) $actionRow->context['exec-time'] === 1)->toBeTrue();
});
