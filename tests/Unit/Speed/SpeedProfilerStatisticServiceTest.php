<?php

use Webspark\Profiling\Enums\StatisticSortingType;
use Webspark\Profiling\Providers\TimingsProfilingProvider;
use Webspark\Profiling\Services\SpeedProfilerStatisticService;
use Webspark\Profiling\Processors\InMemoryProfilingProcessor;
use Webspark\Profiling\Providers\SpeedProfilingProvider;
use Webspark\Profiling\Dto\ProfilerStatisticFilters;
use Webspark\Profiling\Dto\ProfilingRow;

it('can correctly calculate statistic', function () {
    $processor = new InMemoryProfilingProcessor();
    $action = 'testing-statistic';
    $execTimeMax = 8;

    while ($execTimeMax > 0) {
        $processor->write(new ProfilingRow($action, time(), ['exec-time' => $execTimeMax]));
        $execTimeMax--;
    }

    SpeedProfilingProvider::getInstance()->setProcessor($processor);

    $filters = new ProfilerStatisticFilters(10);
    $statisticList = (new SpeedProfilerStatisticService())->generate($filters);

    expect(empty($statisticList[$action]))->toBeFalse()
        ->and(isset($statisticList[$action]['action']))->toBeTrue()
        ->and(isset($statisticList[$action]['max']))->toBeTrue()
        ->and($statisticList[$action]['max'] === 8)->toBeTrue()
        ->and(isset($statisticList[$action]['avg']))->toBeTrue()
        ->and($statisticList[$action]['avg'] === 4.5)->toBeTrue()
        ->and(isset($statisticList[$action]['med']))->toBeTrue()
        ->and($statisticList[$action]['med'] === 4.5)->toBeTrue()
        ->and(isset($statisticList[$action]['min']))->toBeTrue()
        ->and($statisticList[$action]['min'] === 1)->toBeTrue()
        ->and(isset($statisticList[$action]['total']))->toBeTrue()
        ->and($statisticList[$action]['total'] === 36.0)->toBeTrue()
        ->and(isset($statisticList[$action]['calls']))->toBeTrue()
        ->and($statisticList[$action]['calls'] === 8)->toBeTrue();
});

it('throws an exception on incorrect sorting type', function () {
    try {
        $sortingType = new StatisticSortingType('test');
    } catch (Exception $exception) {
        $sortingType = null;
    }

    expect($sortingType === null)->toBeTrue();
});

it('can correctly calculate statistic without sorting', function () {
    $processor = new InMemoryProfilingProcessor();
    $actions = [
        ['name' => 'testing-statistic-first', 'calls' => 10, 'avg' => 5.5, 'med' => 5.5, 'total' => 55.0],
        ['name' => 'testing-statistic-second', 'calls' => 20, 'avg' => 10.5, 'med' => 10.5, 'total' => 210.0],
        ['name' => 'testing-statistic-third', 'calls' => 10, 'avg' => 5.5, 'med' => 5.5, 'total' => 55.0],
    ];

    foreach ($actions as $action) {
        $execTimeMax = $action['calls'];

        while ($execTimeMax > 0) {
            $processor->write(new ProfilingRow($action['name'], time(), ['exec-time' => $execTimeMax]));
            $execTimeMax--;
        }
    }

    SpeedProfilingProvider::getInstance()->setProcessor($processor);

    $filters = new ProfilerStatisticFilters(10, date('Y-m-d'));
    $statisticList = (new SpeedProfilerStatisticService())->generate($filters);

    foreach ($actions as $action) {
        expect(empty($statisticList[$action['name']]))->toBeFalse()
            ->and(isset($statisticList[$action['name']]['action']))->toBeTrue()
            ->and(isset($statisticList[$action['name']]['max']))->toBeTrue()
            ->and($statisticList[$action['name']]['max'] === $action['calls'])->toBeTrue()
            ->and(isset($statisticList[$action['name']]['avg']))->toBeTrue()
            ->and($statisticList[$action['name']]['avg'] === $action['avg'])->toBeTrue()
            ->and(isset($statisticList[$action['name']]['med']))->toBeTrue()
            ->and($statisticList[$action['name']]['med'] === $action['med'])->toBeTrue()
            ->and(isset($statisticList[$action['name']]['min']))->toBeTrue()
            ->and($statisticList[$action['name']]['min'] === 1)->toBeTrue()
            ->and(isset($statisticList[$action['name']]['total']))->toBeTrue()
            ->and($statisticList[$action['name']]['total'] === $action['total'])->toBeTrue()
            ->and(isset($statisticList[$action['name']]['calls']))->toBeTrue()
            ->and($statisticList[$action['name']]['calls'] === $action['calls'])->toBeTrue();
    }
});

it('can correctly calculate several statistics', function () {
    $processor = new InMemoryProfilingProcessor();
    $action = 'testing-statistic';

    $execTimes = [2.0, 4.0, 3.0, 5.0, 4.0, 1.0, 9.0, 6.0, 2.0, 4.0];

    foreach ($execTimes as $execTime) {
        $processor->write(new ProfilingRow($action, time(), ['exec-time' => $execTime]));
    }

    SpeedProfilingProvider::getInstance()->setProcessor($processor);

    $filters = new ProfilerStatisticFilters(10, date('Y-m-d'), new StatisticSortingType(StatisticSortingType::AVG));
    $statisticList = (new SpeedProfilerStatisticService())->generate($filters);

    expect(empty($statisticList[$action]))->toBeFalse()
        ->and(isset($statisticList[$action]['action']))->toBeTrue()
        ->and(isset($statisticList[$action]['max']))->toBeTrue()
        ->and($statisticList[$action]['max'] === 9.00)->toBeTrue()
        ->and(isset($statisticList[$action]['avg']))->toBeTrue()
        ->and($statisticList[$action]['avg'] === 4.0)->toBeTrue()
        ->and(isset($statisticList[$action]['med']))->toBeTrue()
        ->and($statisticList[$action]['med'] === 4.25)->toBeTrue()
        ->and(isset($statisticList[$action]['min']))->toBeTrue()
        ->and($statisticList[$action]['min'] === 1.00)->toBeTrue()
        ->and(isset($statisticList[$action]['total']))->toBeTrue()
        ->and($statisticList[$action]['total'] === 40.0)->toBeTrue()
        ->and(isset($statisticList[$action]['calls']))->toBeTrue()
        ->and($statisticList[$action]['calls'] === 10)->toBeTrue();
});

it('can correctly calculate statistic with timings', function () {
    $timingsProvider = TimingsProfilingProvider::getInstance();
    $timingsProvider->init();

    $processor = new InMemoryProfilingProcessor();
    $action = 'testing-statistic';
    $execTimeMax = 10;

    while ($execTimeMax > 0) {
        $processor->write(new ProfilingRow($action, time(), [
            'exec-time' => $execTimeMax,
            'timings' => $timingsProvider->timings(),
        ]));

        $execTimeMax--;
    }

    SpeedProfilingProvider::getInstance()->setProcessor($processor);

    $filters = new ProfilerStatisticFilters(10);
    $statisticList = (new SpeedProfilerStatisticService())->generate($filters);

    expect(empty($statisticList[$action]))->toBeFalse()
        ->and(isset($statisticList[$action]['action']))->toBeTrue()
        ->and(isset($statisticList[$action]['max']))->toBeTrue()
        ->and($statisticList[$action]['max'] === 10)->toBeTrue()
        ->and(isset($statisticList[$action]['avg']))->toBeTrue()
        ->and($statisticList[$action]['avg'] === 5.5)->toBeTrue()
        ->and(isset($statisticList[$action]['med']))->toBeTrue()
        ->and($statisticList[$action]['med'] === 5.5)->toBeTrue()
        ->and(isset($statisticList[$action]['min']))->toBeTrue()
        ->and($statisticList[$action]['min'] === 1)->toBeTrue()
        ->and(isset($statisticList[$action]['total']))->toBeTrue()
        ->and($statisticList[$action]['total'] === 55.0)->toBeTrue()
        ->and(isset($statisticList[$action]['calls']))->toBeTrue()
        ->and($statisticList[$action]['calls'] === 10)->toBeTrue();
});
