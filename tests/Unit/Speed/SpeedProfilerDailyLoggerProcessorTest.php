<?php

use Webspark\Profiling\Processors\DailyLogProfilingProcessor;
use Webspark\Profiling\Providers\SpeedProfilingProvider;
use Webspark\Profiling\Profilers\SpeedProfiler;
use Webspark\Profiling\Dto\ProfilingRow;
use Webspark\Profiling\ProfilingConfig;

it('can write logs on destruct', function () {
    SpeedProfilingProvider::getInstance()
        ->setProcessor(new DailyLogProfilingProcessor('speed', dirname(__DIR__, 2) . '/logs', 1))
        ->setConfig(new ProfilingConfig(['latency' => 1]));

    speedProfilingCallback('testing', 1000);

    $actionRow = speedProfilingFindRow('testing');

    expect(empty($actionRow))->toBeFalse()
        ->and((int) $actionRow->context['exec-time'] === 1)->toBeTrue();
});

it('can write multiple logs', function () {
    SpeedProfilingProvider::getInstance()
        ->setProcessor(new DailyLogProfilingProcessor('speed', dirname(__DIR__, 2) . '/logs', 1))
        ->setConfig(new ProfilingConfig(['latency' => 1]));

    $expected = $count = 5;

    while ($count > 0) {
        speedProfilingCallback('testing-' . $count, 1000);
        $count--;
    }

    while ($expected > 0) {
        $expectedRow = speedProfilingFindRow('testing-' . $expected);

        expect(empty($expectedRow))->toBeFalse()
            ->and((int) $expectedRow->context['exec-time'] === 1)->toBeTrue();

        $expected--;
    }
});

it('can write logs on checkpoint', function () {
    SpeedProfilingProvider::getInstance()
        ->setProcessor(new DailyLogProfilingProcessor('speed', dirname(__DIR__, 2) . '/logs', 1))
        ->setConfig(new ProfilingConfig(['latency' => 1]));

    $profiler = new SpeedProfiler('testing-checkpoint');

    usleep(1000);
    $profiler->checkpoint(false, 'first');

    usleep(1000);
    $profiler->checkpoint(false, 'second');

    usleep(1000);
    $profiler->checkpoint(false);

    $testingFirstRow = speedProfilingFindRow('testing-checkpoint first');
    $testingSecondRow = speedProfilingFindRow('testing-checkpoint second');
    $testingRow = speedProfilingFindRow('testing-checkpoint');

    expect(empty($testingFirstRow))->toBeFalse()
        ->and((int) $testingFirstRow->context['exec-time'] === 1)->toBeTrue()
        ->and(empty($testingSecondRow))->toBeFalse()
        ->and((int) $testingSecondRow->context['exec-time'] === 2)->toBeTrue()
        ->and(empty($testingRow))->toBeFalse()
        ->and((int) $testingRow->context['exec-time'] === 3)->toBeTrue();
});

it('can rotate logs', function () {
    $folder = dirname(__DIR__, 2) . '/logs/';
    $processor = new DailyLogProfilingProcessor('speed-rotate', $folder, 1);

    $profilingRow = new ProfilingRow('testing-' . time(), time(), ['testing' => true]);
    $processor->write($profilingRow);

    $profilingRow->timestamp = strtotime('-1 day');
    $processor->write($profilingRow);

    $profilingRow->timestamp = strtotime('-2 days');
    $processor->write($profilingRow);

    $processor->rotate();

    $rows = array_filter(
        $processor->rows(),
        static function (ProfilingRow $row): bool {
            return date('Y-m-d', $row->timestamp) === date('Y-m-d', strtotime('-1 day'))
                || date('Y-m-d', $row->timestamp) === date('Y-m-d', strtotime('-2 days'));
        }
    );

    expect(empty($rows))->toBeTrue();
});

it('will rotate on destruct', function () {
    $folder = dirname(__DIR__, 2) . '/logs-delete/';
    $processor = new DailyLogProfilingProcessor('speed-rotate-destruct', $folder, 1);

    $profilingRow = new ProfilingRow('testing-' . time(), time(), ['testing' => true]);
    $processor->write($profilingRow);

    $profilingRow->timestamp = strtotime('-1 day');
    $processor->write($profilingRow);

    $profilingRow->timestamp = strtotime('-2 days');
    $processor->write($profilingRow);

    unset($processor);

    $processor = new DailyLogProfilingProcessor('speed-rotate-destruct', $folder, 1);

    $rows = array_filter(
        $processor->rows(),
        static function (ProfilingRow $row): bool {
            return date('Y-m-d', $row->timestamp) === date('Y-m-d', strtotime('-1 day'))
                || date('Y-m-d', $row->timestamp) === date('Y-m-d', strtotime('-2 days'));
        }
    );

    expect(empty($rows))->toBeTrue();

    foreach (glob($folder . 'speed-rotate*') as $filename) {
        if (is_writable($filename)) {
            unlink($filename);
        }
    }

    rmdir($folder);
});

it('will skip rotate if storing day unlimited', function () {
    $folder = dirname(__DIR__, 2) . '/logs/';

    $processor = new DailyLogProfilingProcessor('speed-rotate-skip', $folder, 0);
    $processor->rotate();

    expect(empty($processor->rows()))->toBeTrue();
});

it('will skip rotate if log files empty', function () {
    $folder = dirname(__DIR__, 2) . '/logs/';

    $processor = new DailyLogProfilingProcessor('speed-rotate-skip', $folder, 2);
    $processor->rotate();

    expect(empty($processor->rows()))->toBeTrue();
});

it('will be empty rows if log file is not file', function () {
    $folder = dirname(__DIR__, 2) . '/logs/';
    $processor = new DailyLogProfilingProcessor('speed-delete', $folder, 1);

    mkdir($folder . 'speed-delete-' . date('Y-m-d', strtotime('+1 day')) . '.log');

    expect(empty($processor->rows()))->toBeTrue();

    foreach (glob($folder . 'speed-delete*') as $filename) {
        if (is_writable($filename)) {
            if (is_dir($filename)) {
                rmdir($filename);
            } else {
                unlink($filename);
            }
        }
    }
});
