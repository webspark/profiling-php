<?php

use Webspark\Profiling\Processors\InMemoryProfilingProcessor;
use Webspark\Profiling\Providers\SpeedProfilingProvider;
use Webspark\Profiling\Profilers\SpeedProfiler;
use Webspark\Profiling\ProfilingConfig;

it('can write logs on destruct', function () {
    SpeedProfilingProvider::getInstance()
        ->setProcessor(new InMemoryProfilingProcessor())
        ->setConfig(new ProfilingConfig(['latency' => 1]));

    speedProfilingCallback('testing', 1000);

    $actionRow = speedProfilingFindRow('testing');

    expect(empty($actionRow))->toBeFalse()
        ->and((int) $actionRow->context['exec-time'] === 1)->toBeTrue();
});

it('can write multiple logs', function () {
    SpeedProfilingProvider::getInstance()
        ->setProcessor(new InMemoryProfilingProcessor())
        ->setConfig(new ProfilingConfig(['latency' => 1]));

    $expected = $count = 5;

    while ($count > 0) {
        speedProfilingCallback('testing-' . $count, 1000);
        $count--;
    }

    while ($expected > 0) {
        $expectedRow = speedProfilingFindRow('testing-' . $expected);

        expect(empty($expectedRow))->toBeFalse()
            ->and((int) $expectedRow->context['exec-time'] === 1)->toBeTrue();

        $expected--;
    }
});

it('can write logs on checkpoint', function () {
    SpeedProfilingProvider::getInstance()
        ->setProcessor(new InMemoryProfilingProcessor())
        ->setConfig(new ProfilingConfig(['latency' => 1]));

    $profiler = new SpeedProfiler('testing');

    usleep(1000);
    $profiler->checkpoint(false, 'first');

    usleep(1000);
    $profiler->checkpoint(false, 'second');

    usleep(1000);
    $profiler->checkpoint(false);

    $testingFirstRow = speedProfilingFindRow('testing first');
    $testingSecondRow = speedProfilingFindRow('testing second');
    $testingRow = speedProfilingFindRow('testing');

    expect(empty($testingFirstRow))->toBeFalse()
        ->and((int) $testingFirstRow->context['exec-time'] === 1)->toBeTrue()
        ->and(empty($testingSecondRow))->toBeFalse()
        ->and((int) $testingSecondRow->context['exec-time'] === 2)->toBeTrue()
        ->and(empty($testingRow))->toBeFalse()
        ->and((int) $testingRow->context['exec-time'] === 3)->toBeTrue();
});
