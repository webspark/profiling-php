<?php

use Webspark\Profiling\Providers\SpeedProfilingProvider;

it('instances equal', function () {
    $providerFirst = SpeedProfilingProvider::getInstance();
    $providerSecond = SpeedProfilingProvider::getInstance();

    expect($providerFirst === $providerSecond)->toBeTrue();
});

it('instance can not be un serialised', function () {
    $provider = SpeedProfilingProvider::getInstance();

    try {
        $serialised = serialize($provider);
        $unSerialised = unserialize($serialised);
    } catch (Exception $exception) {
        $unSerialised = null;
    }

    expect($unSerialised)->toBeNull();
});

it('instance can not be cloned', function () {
    $provider = SpeedProfilingProvider::getInstance();

    try {
        $cloned = clone $provider;
    } catch (Exception $exception) {
        $cloned = null;
    }

    expect($cloned)->toBeNull();
});
