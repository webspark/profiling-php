# Changelog

All notable changes to `profiling-php` will be documented in this file.

## 0.0.1 - 2024-04-02

- Experimental release

## 1.0.0 - 2024-04-15

- First version release

## 1.0.1 - 2024-04-25

- Added median field for speed statistic calculations.

## 1.0.2 - 2024-05-15

- Changed readme.

## 1.0.3 - 2024-07-12

- Fix post params logging for profiling log.

## 1.0.4 - 2024-09-09

- Add incrementing instead of rewriting for methods `start`, `stop` and `setDuration` of `TimingsProfilingProvider`.
- Add configuration set for `TimingsProfilingProvider`.
- Add Introduction and How it works sections to README.md.
- Described `checkpoint` method of `SpeedProfiler` in README.md.
- Add an examples for every using point in README.md.

## 1.0.5 - 2024-09-09

- Add total time spent on action to the profiling statistic.

## 1.0.6 - 2024-09-09

- Add sorting by total for profiling statistic.
